#!/usr/bin/env sh

set -o errexit

command -v curl >/dev/null

ip_address=192.168.100.1

curl --silent http://"$ip_address"/cmAddressData.htm \
    | sed -n "/<TABLE /,/<\/TABLE>/p" \
    | grep "<TD>" \
    | tr -d /"\r" \
    | tr -- - : \
    | sed "s/<TD>//g" \
    | {
        mac_address=
        serial_number=
        tags=model=SB6141
        next_var=
        while read -r line ; do
            if test -z "$next_var" ; then
                case "$line" in
                    Serial\ Number )
                        next_var=serial_number
                        ;;
                    Ethernet\ MAC\ Address )
                        next_var=mac_address
                        ;;
                esac
            else
                eval "$next_var"=\"\$line\"
                next_var=
            fi
        done
        if test -n "$mac_address" ; then
            tags=mac_address="$mac_address"
        fi
        if test -n "$serial_number" ; then
            tags="$tags serial_number=$serial_number"
        fi
        curl --silent http://"$ip_address"/indexData.htm \
            | grep -A 1 "System Up Time" \
            | tail -1 \
            | tr "<>:/[:alpha:]" " " \
            | {
                read -r days hours minutes seconds
                timestamp=$(date +%s)
                hours=$((hours + days * 24))
                minutes=$((minutes + hours * 60))
                seconds=$((seconds + minutes * 60))
                echo "$timestamp $ip_address uptime uptime $seconds $tags"
              }
        curl --silent http://"$ip_address"/cmSignalData.htm \
            | sed -e "s/&nbsp;/ /g" \
                  -e "s@</TABLE>@__END_TABLE__#@g" \
                  -e "s@</TR>@__END_ROW__#@g" \
                  -e "s@<[^>]*>@#@g" \
            | tr -s " #" \
            | tr "#" "\n" \
            | {
                timestamp=$(date +%s)
                direction=
                metric=
                channels=
                channel_idx=1
                power_embedded_table_seen=false
                while read -r line ; do
                    value=
                    if test -z "$line" ; then
                        continue
                    fi
                    case "$line" in
                        Downstream | \
                        Signal\ Status\ \(Codewords\) )
                            direction=downstream
                            ;;
                        Upstream )
                            direction=upstream
                            ;;
                        __END_TABLE__ )
                            if $power_embedded_table_seen && test "$metric" = power ; then
                                channels=
                                direction=
                            else
                                power_embedded_table_seen=true
                            fi
                            ;;
                        * )
                            if test -z "$direction" ; then
                                continue
                            fi
                            value=
                            case "$line" in
                                Channel\ ID )
                                    metric=channel_id
                                    ;;
                                Frequency )
                                    metric=frequency
                                    ;;
                                Power\ Level )
                                    metric=power
                                    ;;
                                Signal\ to\ Noise\ Ratio )
                                    metric=snr
                                    ;;
                                Symbol\ Rate )
                                    metric=symbol_rate
                                    ;;
                                Total\ Unerrored\ Codewords )
                                    metric=unerrored
                                    ;;
                                Total\ Correctable\ Codewords )
                                    metric=corrected
                                    ;;
                                Total\ Uncorrectable\ Codewords )
                                    metric=uncorrectables
                                    ;;
                                __END_ROW__ )
                                    if test "$metric" = power && ! $power_embedded_table_seen ; then
                                        continue
                                    else
                                        metric=
                                        channel_idx=1
                                    fi
                                    ;;
                                1* | 2* | 3* | 4* | 5* | 6* | 7* | 8* | 9* | 0* | -* )
                                    value=$(echo "$line" | cut -f 1 -d " ")
                                    case "$metric" in
                                        channel_id )
                                            if test -z "$channels" ; then
                                                channels="$value"
                                            else
                                                channels="$channels $value"
                                            fi
                                            ;;
                                        freq | \
                                        snr | \
                                        power | \
                                        symbol_rate | \
                                        unerrored | \
                                        corrected | \
                                        uncorrectables )
                                            channel_id=$(echo "$channels" | cut -f "$channel_idx" -d " ")
                                            channel_idx=$(( channel_idx + 1))
                                            echo "$timestamp $ip_address arris_$direction,$channel_id $metric $value $tags"
                                            ;;
                                    esac
                                    ;;
                            esac
                            ;;
                    esac
                done
            }
      }
