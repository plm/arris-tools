#!/usr/bin/env sh

setup_handler() {
    test $# -eq 2
    influx_addr="$1"
    influx_db="$2"
}

push_metrics() {
    while read -r timestamp hostname plugin metric value tags ; do
        plugin=$(echo "$plugin" | sed s/,/,instance=/)
        if test -n "$tags" ; then
            tags=",$(echo "$tags" | tr -s " " ,)"
        fi
        echo "$plugin,host=$hostname,type=$metric$tags value=$value ${timestamp}000000000"
    done | \
        curl "http://$influx_addr/write?db=$influx_db" \
             --silent \
             --request POST \
             --data-binary @-
}
