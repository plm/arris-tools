#!/usr/bin/env sh

setup_handler() {
    if test -z "$interval" ; then
        interval="$COLLECTD_INTERVAL"
    fi
}

push_metrics() {
    while read -r timestamp hostname plugin metric value _ ; do
        plugin=$(echo "$plugin" | tr , /)
        echo "PUTVAL \"$hostname/$plugin/$metric\" interval=$interval $timestamp:$value"
    done
}
