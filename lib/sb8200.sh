#!/usr/bin/env sh

set -o errexit

command -v curl >/dev/null
command -v openssl >/dev/null

password="${ARRIS_PASSWORD:-password}"
ip_address=192.168.100.1

find_table_data() {
    sed -n "/<table class=.simpleTable.>/,/<\/table>/p" \
        | grep -E "<t[dh]" \
        | sed "s@<[^>]*>@@g"
}

parse_uptime() {
    tr ":.[:alpha:]" " " \
        | sed "s@ 0\(.\)@\1@g" \
        | {
            read -r days hours minutes seconds _
            timestamp=$(date +%s)
            hours=$((hours + days * 24))
            minutes=$((minutes + hours * 60))
            seconds=$((seconds + minutes * 60))
            echo "$seconds"
          }
}

auth_token="$(printf %s admin:"$password" \
                  | openssl enc -base64)"
session_token="$(curl --silent \
                      --insecure \
                      https://"$ip_address"/cmconnectionstatus.html?"$auth_token" \
                    | head -1)"
curl --silent \
     --insecure \
     --cookie credential="$session_token" \
     https://"$ip_address"/cmswinfo.html \
    | find_table_data \
    | {
        timestamp=$(date +%s)
        mac_address=
        serial_number=
        uptime=
        next_var=
        tags=model=8200
        while read -r line ; do
            if test -z "$next_var" ; then
                case "$line" in
                    Cable\ Modem\ MAC\ Address )
                        next_var=mac_address
                        ;;
                    Serial\ Number )
                        next_var=serial_number
                        ;;
                    Up\ Time )
                        next_var=uptime
                        ;;
                esac
            else
                if command -V parse_"$next_var" 2>/dev/null | grep -q function ; then
                    eval "$next_var"=\""$(echo "$line" | parse_"$next_var")"\"
                else
                    eval "$next_var"=\"\$line\"
                fi
                next_var=
            fi
        done
        if test -n "$mac_address" ; then
            tags="$tags mac_address=$mac_address"
        fi
        if test -n "$serial_number" ; then
            tags="$tags serial_number=$serial_number"
        fi
        if test -z "$uptime" ; then
            exit 1
        fi
        echo "$timestamp $ip_address uptime uptime $uptime $tags"
        curl --silent \
             --insecure \
             --cookie credential="$session_token" \
             https://"$ip_address"/cmconnectionstatus.html \
            | find_table_data \
            | {
                timestamp=$(date +%s)
                direction=
                channel_id=
                next_field=
                while read -r line ; do
                    case "$line" in
                        Downstream\ Bonded\ Channels | \
                        Upstream\ Bonded\ Channels )
                            direction="$(echo "$line" \
                                             | awk '{print $1}' \
                                             | tr DU du)"
                            next_field=
                            ;;
                    esac
                    if test -z "$next_field" ; then
                        case "$line" in
                            Power )
                                if test upstream = "$direction" ; then
                                    next_field=channel
                                fi
                                ;;
                            Uncorrectables )
                                next_field=channel_id
                                ;;
                        esac
                    else
                        metric="$next_field"
                        value=
                        emit_value() {
                            value="$(echo "$line" | awk '{print $1}')"
                            echo "$timestamp $ip_address arris_$direction,$channel_id $metric $value $tags"
                        }
                        case "$next_field" in
                            channel_id )
                                channel_id="$line"
                                next_field=lock_status
                                ;;
                            channel )
                                next_field=channel_id
                                ;;
                            lock_status )
                                if test downstream = "$direction" ; then
                                    next_field=modulation
                                else
                                    next_field=us_channel_type
                                fi
                                ;;
                            modulation )
                                next_field=frequency
                                ;;
                            us_channel_type )
                                next_field=frequency
                                ;;
                            frequency )
                                if test downstream = "$direction" ; then
                                    next_field=power
                                else
                                    next_field=width
                                fi
                                emit_value
                                ;;
                            power )
                                if test downstream = "$direction" ; then
                                    next_field=snr
                                else
                                    next_field=channel
                                fi
                                emit_value
                                ;;
                            snr )
                                next_field=corrected
                                emit_value
                                ;;
                            corrected )
                                next_field=uncorrectables
                                emit_value
                                ;;
                            uncorrectables )
                                next_field=channel_id
                                emit_value
                                ;;
                            width )
                                next_field=power
                                ;;
                            * )
                                next_field=
                                ;;
                        esac
                    fi
                done
              }
      }
