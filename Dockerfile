ARG ALPINE_VERSION=3.12
ARG VCS_REF
FROM alpine:${ALPINE_VERSION}
LABEL org.label-schema.schema-version=1.0 \
      org.label-schema.name="Arris tools" \
      org.label-schema.vcs-ref="${VCS_REF}"
RUN apk add \
        --no-cache \
        curl \
        openssl
COPY . /arris/
ENTRYPOINT ["/arris/metrics.sh"]
CMD []
