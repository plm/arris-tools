#!/usr/bin/env sh

set -o errexit

lib_dir="$(dirname "$0")/lib"
model=SB8200
interval=
handler=
collector=

while test $# -gt 0 ; do
    case "$1" in
        --interval )
            test -n "$2"
            interval="$2"
            shift 2
            ;;
        --handler )
            test -n "$2"
            handler="$lib_dir/$2.sh"
            test -f "$handler"
            shift 2
            ;;
        --model )
            test -n "$2"
            model="$2"
            shift 2
            ;;
        -- )
            shift 1
            break
            ;;
        -* )
            exit 1
            ;;
        * )
            break
            ;;
    esac
done

if test -n "$model" ; then
    collector="$lib_dir"/"$(echo "$model" | tr "[:upper:]" "[:lower:]")".sh
fi
test -n "$collector"
test -x "$collector"

if test -n "$handler" ; then
    # shellcheck source=lib/collectd.sh
    . "$handler"

    if command -V setup_handler 2>/dev/null | grep -q function ; then
        setup_handler "$@"
    fi
else
    push_metrics() {
        cat
    }
fi

if test -n "$interval" ; then
    start_at=$(($(date +%s) + interval))
    while "$collector" | push_metrics ; do
        now=$(date +%s)
        delay=$((start_at - now))
        if test 0 -le "$delay" ; then
            sleep "$delay"
        fi
        start_at=$((start_at + interval))
    done
else
    "$collector" | push_metrics
fi
