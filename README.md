Metrics Collection Scripts for Arris Cable Modems
===

Basic usage, where your Arris has the IP `192.168.100.1`:

```shell
./metrics.sh --model SB8200
```

This code has been tested against model and firmware:

* SB6141 `SB_KOMODO-1.0.7.4-SCM00-NOSH`
* SB8200 `AB01.02.053.01_112320_193.0A.NSH`

Collectd
---

Usage as a Collectd `Exec` plugin:

```shell
./metrics.sh --model SB8200 --handler collectd
```

Naming of Collectd plugin, types, and instances are inspired by
[jakup's choices](https://github.com/jakup/collectd-python-plugins/blob/master/arris_modem.py)
and can be used with this `types.db`:

```
arris_downstream frequency:GAUGE:0:U, power:GAUGE:U:U, snr:GAUGE:0:U, unerrored:DERIVE:0:U, corrected:DERIVE:0:U, uncorrectables:DERIVE:0:U
arris_upstream symbol_rate:GAUGE:0:U, freq:GAUGE:0:U, power:GAUGE:U:U
```

InfluxDB
---

Using the associated Docker image to push metrics continually to a local InfluxDB instance:

```shell
docker build . --tag arris-tools
docker run --rm arris-tools --model SB8200 --handler influxdb --interval 60 -- localhost:8086 mydb
```

Metrics will be tagged with the modem's ethernet MAC address and serial number.
